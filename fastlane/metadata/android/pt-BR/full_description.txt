Nós oferecemos salas públicas gratuitas de bate-papo para comunidades de desenvolvedores e projetos de código aberto, bem como salas de bate-papo privadas para equipes técnicas e negócios.

CARACTERÍSTICAS PRINCIPAIS

- Salas de bate-papo públicas ilimitadas e gratuitas
- Histórico de bate-papo pesquisável e ilimitado
- Integrações ilimitadas
- Construído sobre a GitHub, a maior rede mundial de desenvolvedores de software
- Salas de bate-papo privadas gratuitas para até 25 usuários

RESPEITADO POR

O Gitter é o lar de mais de 30 mil comunidades de desenvolvedores, incluindo estas: The.NET Foundation, Google Material Design, Angular.js, Backbone, Node.js, Scala, The W3C e muitas outras.

PROBLEMAS? COMENTÁRIOS?

Quanto mais você fala conosco, melhor o Gitter se torna.

Em caso de dúvidas, e para saber mais sobre o produto, visite nosso site de suporte: https://gitter.zendesk.com Você também pode comentar diretamente no canal Gitter HQ: https://gitter.im/orgs/gitterHQ

Diga-nos como podemos melhorar!
